-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity pe_array is
	generic (
		DATA_WIDTH						: integer	:= 32;
		N_BY_N							: integer	:= 2;
		ONLY_POSITIVE_EDGE_TRIGGERABLE	: boolean	:= true
	);
	port (
		Clk			: in	std_logic;
		Rst			: in	std_logic;
		ControlWord	: in	std_logic_vector(3 downto 0);
		in_top		: in	std_logic_vector(DATA_WIDTH*N_BY_N-1 DOWNTO 0);
		in_left		: in	std_logic_vector(DATA_WIDTH*N_BY_N-1 DOWNTO 0);
		out_bottom	: out	std_logic_vector(DATA_WIDTH*N_BY_N-1 DOWNTO 0);
		out_right	: out	std_logic_vector(DATA_WIDTH*N_BY_N-1 DOWNTO 0)
	);
end pe_array;

architecture pe_array_arch of pe_array is
	type vector_type is array(N_BY_N downto 0, N_BY_N downto 0) of std_logic_vector(DATA_WIDTH-1 downto 0);
	signal Xs, Ys : vector_type;

	component pe is
		generic (
			DATA_WIDTH						: integer := DATA_WIDTH;
			ONLY_POSITIVE_EDGE_TRIGGERABLE	: boolean := ONLY_POSITIVE_EDGE_TRIGGERABLE;
			DEBUG_CONNECTIONS				: boolean := false
		);
		Port (
			Clk			: in	std_logic;
			Rst			: in	std_logic;
			ControlWord	: in	std_logic_vector(3 downto 0);
			X, Y		: in	std_logic_vector(DATA_WIDTH-1 downto 0);
			Qx, Qy		: out	std_logic_vector(DATA_WIDTH-1 downto 0);

			DbgInEn	: in	std_logic := '0';
			DbgIAd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgIBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgAd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgIAq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgIBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgAq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-')
		);
	end component;

	signal X1, Y1 : std_logic_vector(DATA_WIDTH*2-1 downto 0);

begin
	ConnectInputsAndOutputs: for i in 0 to N_BY_N-1 generate
		Xs(0, i) <= in_left(DATA_WIDTH*(i+1)-1 downto DATA_WIDTH*i);
		Ys(0, i) <= in_top(DATA_WIDTH*(i+1)-1 downto DATA_WIDTH*i);
		out_right(DATA_WIDTH*(i+1)-1 downto DATA_WIDTH*i) <= Xs(N_BY_N, i);
		out_bottom(DATA_WIDTH*(i+1)-1 downto DATA_WIDTH*i) <= Ys(N_BY_N, i);
	end generate ConnectInputsAndOutputs;

	PeGrid: for i in 0 to N_BY_N-1 generate
		PeGridRow: for j in 0 to N_BY_N-1 generate
			ProcessingElement0: pe
				port map (
					Clk => Clk, Rst => Rst,
					ControlWord => ControlWord,
					X => Xs(j, i),
					Y => Ys(i, j),
					Qx => Xs(j+1, i),
					Qy => Ys(i+1, j)
				);
		end generate PeGridRow;
	end generate PeGrid;
end pe_array_arch;
