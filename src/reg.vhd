-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity reg is
	generic (
		DATA_WIDTH				: integer := 32;
		POSITIVE_EDGE_TRIGGERED	: boolean := true
	);
	port (
		Clk	: in	std_logic;
		WE	: in	std_logic;
		D	: in	std_logic_vector(DATA_WIDTH-1 downto 0);
		Q	: out	std_logic_vector(DATA_WIDTH-1 downto 0)
	);
end reg;

architecture reg_arch of reg is
begin
	process(Clk, WE)
	begin
		if (POSITIVE_EDGE_TRIGGERED) then
			if (Clk'event and Clk = '1' and WE = '1') then
				Q <= D;
			end if;
		else
			if (Clk'event and Clk = '0' and WE = '1') then
				Q <= D;
			end if;
		end if;
	end process;

end reg_arch;
