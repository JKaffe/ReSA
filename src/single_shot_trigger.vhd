-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

-- Implementing first design: https://electronics.stackexchange.com/a/304196

-- After En was high for one clock cycle starting in a rising edge, Q will go
-- high the following clock cycle.
-- Q will then stay low until En is low for one clock cycle.
-- Rst is used to put the circuit in a know state. You don't need to use it as
-- En held low for 3 clock cycles achieves the same.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity single_shot_trigger is
	port (
		Clk, En	: in	std_logic;
		Rst		: in	std_logic := '0';
		Q		: out	std_logic
	);
end single_shot_trigger;

architecture single_shot_trigger_arch of single_shot_trigger is
	signal q1, q2, q3, q3n : std_logic;
begin
	process(Clk, Rst, En)
	begin
		if (Rst = '1') then
			q1 <= '0';
		elsif (Clk'event and Clk = '1') then
			q1 <= En;
		end if;
	end process;

	process(Clk, Rst, q1)
	begin
		if (Rst = '1') then
			q2 <= '0';
		elsif (Clk'event and Clk = '1') then
			q2 <= q1;
		end if;
	end process;

	process(Clk, Rst, q2)
	begin
		if (Rst = '1') then
			q3 <= '0';
			q3n <= '1';
		elsif (Clk'event and Clk = '1') then
			q3 <= q2;
			q3n <= not q2;
		end if;
	end process;

	Q <= q2 and q3n;
end single_shot_trigger_arch;
