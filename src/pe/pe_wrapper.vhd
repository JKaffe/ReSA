-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pe_wrapper is
	generic (
		DATA_WIDTH						: integer := 32;
		ONLY_POSITIVE_EDGE_TRIGGERABLE	: boolean := true;
		DEBUG_CONNECTIONS				: boolean := true
	);
	Port (
		Clk			: in	std_logic;
		Rst			: in	std_logic;
		ControlWord	: in	std_logic_vector(3 downto 0);
		X, Y		: in	std_logic_vector(DATA_WIDTH-1 downto 0);
		Qx, Qy		: out	std_logic_vector(DATA_WIDTH-1 downto 0);

		DbgInEn	: in	std_logic;
		DbgIAd, DbgIBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0);
		DbgAd, DbgBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0);
		DbgIAq, DbgIBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0);
		DbgAq, DbgBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0)
	);
end pe_wrapper;

architecture pe_wrapper_arch of pe_wrapper is
	component pe is
		generic (
			DATA_WIDTH						: integer := 32;
			ONLY_POSITIVE_EDGE_TRIGGERABLE	: boolean := true;
			DEBUG_CONNECTIONS				: boolean := true
		);
		Port (
			Clk			: in	std_logic;
			Rst			: in	std_logic;
			ControlWord	: in	std_logic_vector(3 downto 0);
			X, Y		: in	std_logic_vector(DATA_WIDTH-1 downto 0);
			Qx, Qy		: out	std_logic_vector(DATA_WIDTH-1 downto 0);

			DbgInEn	: in	std_logic := '0';
			DbgIAd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => 'X');
			DbgIBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => 'X');
			DbgAd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => 'X');
			DbgBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => 'X');
			DbgIAq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => 'X');
			DbgIBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => 'X');
			DbgAq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => 'X');
			DbgBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => 'X')
		);
	end component;
begin
	ProcessingElement: pe
		generic map (DATA_WIDTH => DATA_WIDTH, DEBUG_CONNECTIONS => DEBUG_CONNECTIONS)
		port map (
			Clk => Clk, Rst => Rst,
			ControlWord => ControlWord,
			X => X, Y => Y, Qx => Qx, Qy=> Qy,
			DbgInEn => DbgInEn,
			DbgIAd => DbgIAd, DbgIBd => DbgIBd, DbgAd => DbgAd, DbgBd => DbgBd,
			DbgIAq => DbgIAq, DbgIBq => DbgIBq, DbgAq => DbgAq, DbgBq => DbgBq
		);

end pe_wrapper_arch;
