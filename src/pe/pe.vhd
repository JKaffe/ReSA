-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pe is
	generic (
		DATA_WIDTH						: integer := 32;
		-- Zedboard does not support triggering flip-flops on falling edge.
		-- As far as my testing demonstrated.
		ONLY_POSITIVE_EDGE_TRIGGERABLE	: boolean := true;
		DEBUG_CONNECTIONS				: boolean := true
	);
	Port (
		Clk			: in	std_logic;
		Rst			: in	std_logic;
		ControlWord	: in	std_logic_vector(3 downto 0);
		X, Y		: in	std_logic_vector(DATA_WIDTH-1 downto 0);
		Qx, Qy		: out	std_logic_vector(DATA_WIDTH-1 downto 0);

		DbgInEn	: in	std_logic := '0';
		DbgIAd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
		DbgIBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
		DbgAd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
		DbgBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
		DbgIAq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
		DbgIBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
		DbgAq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
		DbgBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-')
	);
end pe;

architecture pe_arch of pe is
	component reg is
		generic (
			DATA_WIDTH				: integer := 32;
			POSITIVE_EDGE_TRIGGERED	: boolean := true
		);
		port (
			Clk	: in	std_logic;
			WE	: in	std_logic;
			D	: in	std_logic_vector(DATA_WIDTH-1 downto 0);
			Q	: out	std_logic_vector(DATA_WIDTH-1 downto 0)
		);
	end component;

	signal WEExtReg, WEIntReg			: std_logic;
	signal IAd, IAq, IBd, IBq			: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal IAd2, Ad2, IBd2, Bd2			: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal Ad, Aq, Bd, Bq				: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal SMIH, SMA0, SMR0, SMR1, SMO	: std_logic;
	signal BOpIn, R0Mux0q				: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal operandA, operandB			: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal IAgreaterThanIB				: boolean;
	signal add, lower, higher, res		: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal mul							: std_logic_vector(2*DATA_WIDTH-1 downto 0);
begin
	-- PE registers
	IAReg: reg
		generic map (DATA_WIDTH => DATA_WIDTH)
		port map (Clk => Clk, WE => WEExtReg, D => IAd2, Q => IAq);

	IBReg: reg
		generic map (DATA_WIDTH => DATA_WIDTH)
		port map (Clk => Clk, WE => WEExtReg, D => IBd2, Q => IBq);

	AReg: reg
		generic map (DATA_WIDTH => DATA_WIDTH,
					POSITIVE_EDGE_TRIGGERED => ONLY_POSITIVE_EDGE_TRIGGERABLE)
		port map (Clk => Clk, WE => WEIntReg, D => Ad2, Q => Aq);

	BReg: reg
		generic map (DATA_WIDTH => DATA_WIDTH,
					POSITIVE_EDGE_TRIGGERED => ONLY_POSITIVE_EDGE_TRIGGERABLE)
		port map (Clk => Clk, WE => WEIntReg, D => Bd2, Q => Bq);

	-- Debug capability
	process(IAd, IAq, IBd, IBq, Ad, Aq, Bd, Bq, DbgInEn, DbgIAd, DbgIBd, DbgAd, DbgBd)
	begin
		if (DEBUG_CONNECTIONS) then
			if (DbgInEn = '0') then
				IAd2 <= IAd;
				IBd2 <= IBd;
				Ad2 <= Ad;
				Bd2 <= Bd;
			else
				IAd2 <= DbgIAd;
				IBd2 <= DbgIBd;
				Ad2 <= DbgAd;
				Bd2 <= DbgBd;
			end if;

			DbgIAq <= IAq;
			DbgIBq <= IBq;
			DbgAq <= Aq;
			DbgBq <= Bq;
		else
			IAd2 <= IAd;
			IBd2 <= IBd;
			Ad2 <= Ad;
			Bd2 <= Bd;

			DbgIAq <= (others => '-');
			DbgIBq <= (others => '-');
			DbgAq <= (others => '-');
			DbgBq <= (others => '-');
		end if;
	end process;


	-- If flip-flops cannot be triggered on falling edge, take two cycles.
	-- First rising edge writes IA, IB, and second rising writes A, B.
	-- If falling edge triggerable, A and B are written to on the falling edge.
	process(Clk, Rst)
	begin
		if (ONLY_POSITIVE_EDGE_TRIGGERABLE) then
			if (Rst = '1') then
				WEIntReg <= '0';
				WEExtReg <= '1';
			elsif (Clk'event and Clk = '1') then
				WEIntReg <= not WEIntReg;
				WEExtReg <= WEIntReg;
			end if;
		else
			WEExtReg <= '1';
			WEIntReg <= '1';
		end if;
	end process;

	-- Multiplexer for holding IA and IB
	SMIH <= (SMR0 and ControlWord(0)) or (ControlWord(3) and ControlWord(1));
	IAd <= X when SMIH = '0' else IAq;
	IBd <= Y when SMIH = '0' else IBq;

	-- Multiplexer selecting internal (A, B) or external (IA, IB) registers.
	operandA <= IAq when ControlWord(0) = '0' else Aq;
	operandB <= IBq when ControlWord(0) = '0' else Bq;

	-- OPs
	add <= std_logic_vector(unsigned(operandA) + unsigned(operandB));
	mul <= std_logic_vector(unsigned(operandA) * unsigned(operandB));
	IAgreaterThanIB <= unsigned(IAq) > unsigned(IBq);
	lower <= IBq when IAgreaterThanIB else IAq;
	higher <= IAq when IAgreaterThanIB else IBq;

	-- OPMux
	with (ControlWord(3 downto 2)) select
		res <= mul(DATA_WIDTH-1 downto 0)	when "00",
			   add							when "01",
			   lower						when "10",
			   Aq							when others;

	-- Res out
	SMA0 <= (not ControlWord(3)) and ControlWord(1);
	RegOutDemux: process(SMA0, Aq, res)
	begin
		if (SMA0 = '0') then
			Ad <= res;
			BOpIn <= (others => '-');
		else
			Ad <= Aq;
			BOpIn <= res;
		end if;
	end process;

	-- R0 mux 0
	SMR0 <= not ControlWord(3);
	R0Mux0q <= higher when SMR0 = '0' else BOpIn;

	-- R0 mux 1
	SMR1 <= SMA0 or ((not ControlWord(2)) and ControlWord(3));
	Bd <= Bq when SMR1 = '0' else R0Mux0q;
	
	-- Output muxes
	SMO <= ControlWord(3) and ControlWord(2);
	Qx <= (others => '0') when SMO = '0' else operandA;
	Qy <= (others => '0') when SMO = '0' else operandB;

end pe_arch;
