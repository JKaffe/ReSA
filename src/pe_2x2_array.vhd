-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity pe_2x2_array is
	generic (
		DATA_WIDTH	: integer := 32
	);
	port (
		Clk			: in	std_logic;
		Rst			: in	std_logic;
		ControlWord	: in	std_logic_vector(3 downto 0);
		in_top		: in	std_logic_vector(DATA_WIDTH*2-1 DOWNTO 0);
		in_left		: in	std_logic_vector(DATA_WIDTH*2-1 DOWNTO 0);
		out_bottom	: out	std_logic_vector(DATA_WIDTH*2-1 DOWNTO 0);
		out_right	: out	std_logic_vector(DATA_WIDTH*2-1 DOWNTO 0)
	);
end pe_2x2_array;

architecture pe_2x2_array_arch of pe_2x2_array is
	component pe is
		generic (
			DATA_WIDTH						: integer := DATA_WIDTH;
			ONLY_POSITIVE_EDGE_TRIGGERABLE	: boolean := true;
			DEBUG_CONNECTIONS				: boolean := false
		);
		Port (
			Clk			: in	std_logic;
			Rst			: in	std_logic;
			ControlWord	: in	std_logic_vector(3 downto 0);
			X, Y		: in	std_logic_vector(DATA_WIDTH-1 downto 0);
			Qx, Qy		: out	std_logic_vector(DATA_WIDTH-1 downto 0);

			DbgInEn	: in	std_logic := '0';
			DbgIAd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgIBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgAd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgIAq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgIBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgAq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-')
		);
	end component;

	signal X1, Y1 : std_logic_vector(DATA_WIDTH*2-1 downto 0);

begin
	ProcessingElement0: pe
		port map (
			Clk => Clk, Rst => Rst,
			ControlWord => ControlWord,
			X => in_left(DATA_WIDTH-1 downto 0),
			Y => in_top(DATA_WIDTH-1 downto 0),
			Qx => X1(DATA_WIDTH-1 downto 0),
			Qy => Y1(DATA_WIDTH-1 downto 0)
		);

	ProcessingElement1: pe
		port map (
			Clk => Clk, Rst => Rst,
			ControlWord => ControlWord,
			X => X1(DATA_WIDTH-1 downto 0),
			Y => in_top(DATA_WIDTH*2-1 downto DATA_WIDTH),
			Qx => out_right(DATA_WIDTH-1 downto 0),
			Qy => Y1(DATA_WIDTH*2-1 downto DATA_WIDTH)
		);

	ProcessingElement2: pe
		port map (
			Clk => Clk, Rst => Rst,
			ControlWord => ControlWord,
			X => in_left(DATA_WIDTH*2-1 downto DATA_WIDTH),
			Y => Y1(DATA_WIDTH-1 downto 0),
			Qx => X1(DATA_WIDTH*2-1 downto DATA_WIDTH),
			Qy => out_bottom(DATA_WIDTH-1 downto 0)
		);

	ProcessingElement3: pe
		port map (
			Clk => Clk, Rst => Rst,
			ControlWord => ControlWord,
			X => X1(DATA_WIDTH*2-1 downto DATA_WIDTH),
			Y => Y1(DATA_WIDTH*2-1 downto DATA_WIDTH),
			Qx => out_right(DATA_WIDTH*2-1 downto DATA_WIDTH),
			Qy => out_bottom(DATA_WIDTH*2-1 downto DATA_WIDTH)
		);
end pe_2x2_array_arch;
