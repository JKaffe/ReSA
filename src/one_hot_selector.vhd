-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity one_hot_selector is
	generic (
		SIZE	: integer := 8
	);
	port (
		Clk	: in	std_logic;
		Rst	: in	std_logic;
		Q	: out	std_logic_vector(SIZE-1 downto 0)
	);
end one_hot_selector;

architecture one_hot_selector_arch of one_hot_selector is
	signal Ds, Qs : std_logic_vector(SIZE-1 downto 0);
begin
	CreateRegister: process(Clk, Rst)
	begin
		if (Rst = '1') then
			Qs(0) <= '1';
			Qs(SIZE-1 downto 1) <= (others => '0');
		elsif (Clk'event and Clk = '1') then
			Qs <= Ds;
		end if;
	end process;

	Q <= Qs;

	Ds(0) <= Qs(SIZE-1);
	Ds(SIZE-1 downto 1) <= Qs(SIZE-2 downto 0);

end one_hot_selector_arch;
