# Reprogrammable Systolic Array (ReSA).

## Status.
In development.

## Licence.
The HDL is licensed under the CERN OHL-S (strongly-reciprocal) v2 (unless otherwise noted), or (at your option) any later version.


The C code is licensed under the GPL v3.0 (unless otherwise noted), or (at you option) any later version.


Svg, pdf files and other diagram/illustrations are licensed under the CC BY-SA 4.0 (unless otherwise noted).
