-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity reg_tb is
end reg_tb;

architecture testbench of reg_tb is
	component reg is
		generic (
			DATA_WIDTH				: integer := 32;
			POSITIVE_EDGE_TRIGGERED	: boolean := true
		);
		port (
			Clk	: in	std_logic;
			WE	: in	std_logic;
			D	: in	std_logic_vector(DATA_WIDTH-1 downto 0);
			Q	: out	std_logic_vector(DATA_WIDTH-1 downto 0)
		);
	end component;

	constant DATA_WIDTH	: integer := 32;
	constant CLK_PERIOD	: time := 10ns;

	signal Clk				: std_logic;
	signal D0, D1, Q0, Q1	: std_logic_vector(DATA_WIDTH-1 downto 0);
begin
	Clock: process
	begin
		Clk <= '0';
		wait for CLK_PERIOD/2;
		Clk <= '1';
		wait for CLK_PERIOD/2;
	end process;

	PositiveEdgeReg: reg
		generic map (DATA_WIDTH => DATA_WIDTH, POSITIVE_EDGE_TRIGGERED => true)
		port map (Clk => Clk, WE => '1', D => D0, Q => Q0);

	NegativeEdgeReg: reg
		generic map (DATA_WIDTH => DATA_WIDTH, POSITIVE_EDGE_TRIGGERED => false)
		port map (Clk => Clk, WE => '1', D => D1, Q => Q1);

	Stimuli: process
	begin
		D0 <= (others => '0');
		wait for CLK_PERIOD/2;

		D1 <= (others => '0');
		wait for CLK_PERIOD/2;

		D0 <= (others => '1');
		wait for CLK_PERIOD/2;

		D1 <= (others => '1');
		wait for CLK_PERIOD/2;

		wait;
	end process;
end testbench;
