-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity single_shot_trigger_tb is
end single_shot_trigger_tb;

architecture testbench of single_shot_trigger_tb is
	constant CLK_PERIOD : time := 10ns;

	component single_shot_trigger is
		port (
			Clk, Rst, En	: in	std_logic;
			Q				: out	std_logic
		);
	end component;

	signal Clk, Rst, En, Q : std_logic;
begin
	UUT: single_shot_trigger port map (Clk => Clk, Rst => Rst, En => En, Q => Q);

	Clock: process
	begin
		Clk <= '0';
		wait for CLK_PERIOD/2;
		Clk <= '1';
		wait for CLK_PERIOD/2;
	end process;

	process
	begin
		Rst <= '1';
		En <= '0';
		wait for CLK_PERIOD/4;
		Rst <= '0';
		wait for CLK_PERIOD/4;
		wait for CLK_PERIOD/2;

		wait for 2*CLK_PERIOD;

		En <= '1';
		wait for 5*CLK_PERIOD;

		En <= '0';
		wait for 5*CLK_PERIOD;

		En <= '1';
		wait for 1*CLK_PERIOD;

		En <= '0';
		wait for 5*CLK_PERIOD;

		wait for CLK_PERIOD/2;
		-- Rising edge
		wait for CLK_PERIOD/4;
		-- Middle of high
		En <= '1';
		wait for CLK_PERIOD/4;
		-- Falling edge
		wait for CLK_PERIOD/2;

		wait for CLK_PERIOD;
		En <= '0';

		wait for 4* CLK_PERIOD;


	end process;
end testbench;
