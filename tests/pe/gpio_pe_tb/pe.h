// PackageName: ReSA
// SPDX-FileCopyrightText: 2022 Karmjit Mahil
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef _PE_HEADDER
#define _PE_HEADDER

#include <stdbool.h>
#include <stdint.h>

#define GPIO_DATA_REG	((uint32_t) 0u)
#define GPIO_TRI_REG	((uint32_t) 0x4u)
#define GPIO2_DATA_REG	((uint32_t) 0x8u)
#define GPIO2_TRI_REG	((uint32_t) 0xCu)

#define GPIO_ALL_READ_MASK	((uint32_t) 0u)
#define GPIO_ALL_WRITE_MASK	((uint32_t) ~0u)

#define CLK_PERIOD 40

#define CLK_MASK				((uint32_t) 1u << 4)
#define RESET_MASK				((uint32_t) 1u << 5)
#define DEBUG_WRITE_REGS_MASK	((uint32_t) 1u << 6)

bool pe_construct(int mem_fd, bool debuggable_pe);

bool pe_destruct();

void set_cw(uint32_t control_word);

void set_xy(uint32_t x, uint32_t y);

void get_qxqy(uint32_t *qx, uint32_t *qy);

bool get_regs(uint32_t *iaq, uint32_t *ibq, uint32_t *aq, uint32_t *bq);

bool force_regs(uint32_t iad, uint32_t ibd, uint32_t ad, uint32_t bd);

void reset_pulse();

void clock_pulse();

#endif
