// PackageName: ReSA
// SPDX-FileCopyrightText: 2022 Karmjit Mahil
// SPDX-License-Identifier: GPL-3.0-or-later

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdio_ext.h>
#include <string.h>
#include <unistd.h>

#include "pe.h"

int main() {
	char tok[10] = {'\0'};
	bool debuggable_pe = true;
	uint32_t hex_lit1, hex_lit2, hex_lit3, hex_lit4;

	errno = 0;
	int mem_fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (mem_fd == -1) {
		perror("Could not open /dev/mem\n");
		return 1;
	}

	if(!pe_construct(mem_fd, debuggable_pe)) {
		fprintf(stderr, "PE initialisation failed.");
		close(mem_fd);
		return 1;
	}

	for(;;) {
		printf("> ");
		fflush(stdout);

		fgets(tok, 10, stdin);
		for (int i = 0; tok[i] != '\0'; i++){
			if (tok[i] == '\n') {
				tok[i] = '\0';
				break;
			}
		}

		if (!strcmp(tok, "cw")) {
			printf("CW (0x[0-9a-fA-F]): ");
			fflush(stdout);


			if (scanf("0x%1X", &hex_lit1) != 1) {
				fprintf(stderr, "\nInvalid control word.\n");
				continue;
			}
			__fpurge(stdin);

			printf("Setting: CW: 0x%X\n", hex_lit1);
			set_cw(hex_lit1);

		
		} else if (!strcmp(tok, "exec")) {
			printf("Sending clock pulse.\n");
			clock_pulse();
			printf("Sending clock pulse.\n");
			clock_pulse();

			get_qxqy(&hex_lit1, &hex_lit2);
			printf("Qx: 0x%08X, Qy: 0x%08X\n", hex_lit1, hex_lit2);

		} else if (!strcmp(tok, "exit")) {
			printf("Exiting shell.\n");
	   		break;
		} else if (!strcmp(tok, "force")) {
			printf("IA (0x[0-9a-fA-F]{8}): ");

			if (scanf("0x%8x", &hex_lit1) != 1) {
				printf("Input error: %s", tok);
				continue;
			}
			__fpurge(stdin);

			printf("IB (0x[0-9a-fA-F]{8}): ");
			if (scanf("0x%8x", &hex_lit2) != 1) {
				printf("Input error.");
				continue;
			}
			__fpurge(stdin);

			printf("A (0x[0-9a-fA-F]{8}): ");
			if (scanf("0x%8x", &hex_lit3) != 1) {
				printf("Input error.");
				continue;
			}
			__fpurge(stdin);

			printf("B (0x[0-9a-fA-F]{8}): ");
			if (scanf("%8x", &hex_lit4) != 1) {
				printf("Input error.");
				continue;
			}
			__fpurge(stdin);

			printf("Setting:\n"
				   "   IA: 0x%08X, IB: 0x%08X\n"
				   "    A: 0x%08X,  B: 0x%08X\n",
				   hex_lit1, hex_lit2, hex_lit3, hex_lit4);

			force_regs(hex_lit1, hex_lit2, hex_lit3, hex_lit4);

		} else if (!strcmp(tok, "in")) {
			printf("X (0x[0-9a-fA-F]{8}): ");

			if (scanf("0x%8x", &hex_lit1) != 1) {
				printf("Input error.");
				continue;
			}
			__fpurge(stdin);

			printf("Y (0x[0-9a-fA-F]{8}): ");
			if (scanf("0x%8x", &hex_lit2) != 1) {
				printf("Input error.");
				continue;
			}
			__fpurge(stdin);

			printf("Setting: Qx: 0x%08X, Qy: 0x%08X\n", hex_lit1, hex_lit2);
			set_xy(hex_lit1, hex_lit2);
		
		} else if (!strcmp(tok, "out")) {
			get_qxqy(&hex_lit1, &hex_lit2);
			printf("Qx: 0x%08X, Qy: 0x%08X", hex_lit1, hex_lit2);

		} else if (!strcmp(tok, "regs")) {
			get_regs(&hex_lit1, &hex_lit2, &hex_lit3, &hex_lit4);
			printf("IA: 0x%08X, IB: 0x%08X\n"
				   " A: 0x%08X,  B: 0x%08X\n",
				   hex_lit1, hex_lit2, hex_lit3, hex_lit4);

		} else if (!strcmp(tok, "rst")) {
			printf("Sending reset pulse.\n");
			reset_pulse();

		} else if (!strcmp(tok, "help")) {
			printf("Commands:\n"
				   "    cw      Set control word.\n"
				   "    in      Set X and Y inputs.\n"
				   "    exec    Execute operation.\n"
				   "    out     Get Qx and Qy outputs.\n"
				   "    force   Forcibly set IA, IB, A, and B registers.\n"
				   "    regs    Get IA, IB, A, B registers.\n"
				   "    exit    Exit shell.\n");

		} else {
			printf("Unknown command.\n");
		}

	}
	pe_destruct();

	close(mem_fd);

	return 0;
}
