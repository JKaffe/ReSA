// PackageName: ReSA
// SPDX-FileCopyrightText: 2022 Karmjit Mahil
// SPDX-License-Identifier: GPL-3.0-or-later

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include "pe.h"
#include "gpio_pe_test_set.h"

extern const struct test_data test_set[];


/* Execute command PE cw, with reset selection parameter rst to reset the PE. */
static void execrs(uint32_t cw, bool rst,
				   uint32_t x, uint32_t y,
				   uint32_t *qx, uint32_t *qy)
{
	assert(qx != NULL && qy != NULL && qx != qy);

	printf("Setting: CW: 0x%X\n", cw);
	set_cw(cw);

	printf("Setting:  X: 0x%08X,  Y: 0x%08X\n", x, y);
	set_xy(x, y);


	if (rst) {
		printf("Sending reset pulse.\n");
		reset_pulse();
	}

	printf("Sending clock pulse.\n");
	clock_pulse();
	printf("Sending clock pulse.\n");
	clock_pulse();

	get_qxqy(qx, qy);
	printf("Read:    Qx: 0x%08X, Qy: 0x%08X\n", *qx, *qy);

}

static inline void
exec(uint32_t cw, uint32_t x, uint32_t y, uint32_t *qx, uint32_t *qy) {
	execrs(cw, false, x, y, qx, qy);
}

static inline void
execr(uint32_t cw, uint32_t x, uint32_t y, uint32_t *qx, uint32_t *qy) {
	execrs(cw, true, x, y, qx, qy);
}

int main() {
	uint32_t qx, qy;
	uint32_t iaq, ibq, aq, bq;
	uint32_t failed = 0u, total = sizeof(test_set)/sizeof(test_set[0]);
	const bool debuggable_pe = true;

	errno = 0;
	int mem_fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (mem_fd == -1) {
		perror("Could not open /dev/mem\n");
		return 1;
	}

	if(!pe_construct(mem_fd, debuggable_pe)) {
		fprintf(stderr, "PE initialisation failed.");
		close(mem_fd);
		return 1;
	}


	for(int i = 0; i < total; i++) {
		printf("\n");
		if (i == 0)
			execr(test_set[i].cw, test_set[i].x, test_set[i].y, &qx, &qy);
		else
			exec(test_set[i].cw, test_set[i].x, test_set[i].y, &qx, &qy);


		if (qx != test_set[i].qx || qy != test_set[i].qy) {
			fprintf(stderr,
					"\033[0;31mActual outputs differ from expected:\033[0m\n"
					"\tTest: %d\tCW: 0x%X\n"
					"\tActual\t\tQx: 0x%08X, Qy: 0x%08X\n"
					"\tExpected\tQx: 0x%08X, Qy: 0x%08X\n",
					i + 1, test_set[i].cw, qx, qy, test_set[i].qx, test_set[i].qy);
			if (debuggable_pe) {
				get_regs(&iaq, &ibq, &aq, &bq);
				fprintf(stderr,
						"\tRegisters:\n"
						"\t\t\tIA: 0x%08X, IB: 0x%08X\n"
						"\t\t\t A: 0x%08X,  B: 0x%08X\n",
						iaq, ibq, aq, bq);
			}
			failed++;
		}
	}

	printf("\nTests completed.\nTotal: %u, Failed: %u\n", total, failed);

	pe_destruct();

	close(mem_fd);

	return 0;
}
