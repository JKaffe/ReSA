// PackageName: ReSA
// SPDX-FileCopyrightText: 2022 Karmjit Mahil
// SPDX-License-Identifier: GPL-3.0-or-later

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <time.h>

#include "pe.h"

static struct processing_element {
	bool debuggable;
	volatile uint32_t *config_reg;
	volatile uint32_t *x, *y, *qx, *qy;
	volatile uint32_t *iaq, *ibq, *aq, *bq;
	volatile uint32_t *iad, *ibd, *ad, *bd;
} pe;

struct axi_node {
	const char *name;
	uint32_t *addr;
	uint32_t *mapped_addr;
	const uint32_t physical_addr;
	const uint32_t size;
};

static struct axi_node config_node = {"Axi GPIO Configuration interface",
									  MAP_FAILED, MAP_FAILED, 0x41200000,
									  64 * 1024,};

static struct axi_node xy_node = {"Axi GPIO PE X Y node",
								  MAP_FAILED, MAP_FAILED, 0x41210000,
								  64 * 1024};

static struct axi_node qxqy_node = {"Axi GPIO PE Qx Qy node",
									MAP_FAILED, MAP_FAILED, 0x41220000,
									64 * 1024};

static struct axi_node iaqibq_node = {"Axi GPIO PE Debug IAq IBq",
									  MAP_FAILED, MAP_FAILED, 0x41230000,
									  64 * 1024};

static struct axi_node aqbq_node = {"Axi GPIO PE Debug Aq Bq",
									MAP_FAILED, MAP_FAILED, 0x41240000,
									64 * 1024};

static struct axi_node iadibd_node = {"Axi GPIO PE Debug IAd IBd",
									  MAP_FAILED, MAP_FAILED, 0x41250000,
									  64 * 1024};

static struct axi_node adbd_node = {"Axi GPIO PE Debug Ad Bd",
									MAP_FAILED, MAP_FAILED, 0x41260000,
									64 * 1024};


static bool axi_mmap(int mem_fd, struct axi_node * const axi) {
	const uint32_t PAGE_ALLIGN_MASK = (uint32_t) (sysconf(_SC_PAGE_SIZE) - 1);

	assert(axi != NULL);
	assert(axi -> mapped_addr == MAP_FAILED /* axi node already mapped. */);

	errno = 0;
	axi -> mapped_addr = (uint32_t *) mmap(0, axi -> size,
			PROT_READ | PROT_WRITE,
			MAP_SHARED, mem_fd,
			axi -> physical_addr & ~PAGE_ALLIGN_MASK);
	if (axi -> mapped_addr == MAP_FAILED)
		return false;

	axi -> addr = axi -> mapped_addr;
	axi -> addr += axi-> physical_addr & PAGE_ALLIGN_MASK;
	return true;
}

static bool axi_munmap(struct axi_node * const axi) {
	assert(axi != NULL);
	assert(axi -> mapped_addr != MAP_FAILED && axi -> addr != MAP_FAILED
					/* Never mapped or trying to unmap twice. */);

	errno = 0;
	if (munmap(axi -> mapped_addr, axi -> size) == -1)
		return false;

	axi -> mapped_addr = MAP_FAILED;
	axi -> addr = MAP_FAILED;

	return true;
}

bool pe_construct(int mem_fd, bool debuggable_pe) {
	pe.debuggable = debuggable_pe;

	if (!axi_mmap(mem_fd, &config_node)) {
		printf("Failed mmap. Interface: %s.\n%s\n", config_node.name,
				strerror(errno));
		goto MmapConfigFailed;
	}
	*(config_node.addr + GPIO_TRI_REG) = GPIO_ALL_WRITE_MASK;
	pe.config_reg = config_node.addr + GPIO_DATA_REG;
	printf("%s - mapped at: %p\n", config_node.name, config_node.addr);

	if (!axi_mmap(mem_fd, &xy_node)) {
		printf("Failed mmap. Interface: %s.\n%s\n.", xy_node.name,
				strerror(errno));
		goto MmapXYFailed;
	}
	*(xy_node.addr + GPIO_TRI_REG) = GPIO_ALL_WRITE_MASK;
	*(xy_node.addr + GPIO2_TRI_REG) = GPIO_ALL_WRITE_MASK;
	pe.x = xy_node.addr + GPIO_DATA_REG;
	pe.y = xy_node.addr + GPIO2_DATA_REG;
	printf("%s - mapped at: %p\n", xy_node.name, xy_node.addr);

	if (!axi_mmap(mem_fd, &qxqy_node)) {
		printf("Failed mmap. Interface: %s.\n%s\n.", qxqy_node.name,
				strerror(errno));
		goto MmapQxQyFailed;
	}
	*(qxqy_node.addr + GPIO_TRI_REG) = GPIO_ALL_READ_MASK;
	*(qxqy_node.addr + GPIO2_TRI_REG) = GPIO_ALL_READ_MASK;
	pe.qx = qxqy_node.addr + GPIO_DATA_REG;
	pe.qy = qxqy_node.addr + GPIO2_DATA_REG;
	printf("%s - mapped at: %p\n", qxqy_node.name, qxqy_node.addr);

	if (debuggable_pe) {
		if (!axi_mmap(mem_fd, &iaqibq_node)) {
			printf("Failed mmap. Interface: %s.\n%s\n.", iaqibq_node.name,
					strerror(errno));
			goto MmapIAqIBqFailed;
		}
		*(iaqibq_node.addr + GPIO_TRI_REG) = GPIO_ALL_READ_MASK;
		*(iaqibq_node.addr + GPIO2_TRI_REG) = GPIO_ALL_READ_MASK;
		pe.iaq = iaqibq_node.addr + GPIO_DATA_REG;
		pe.ibq = iaqibq_node.addr + GPIO2_DATA_REG;
		printf("%s - mapped at: %p\n", iaqibq_node.name, iaqibq_node.addr);

		if (!axi_mmap(mem_fd, &aqbq_node)) {
			printf("Failed mmap. Interface: %s.\n%s\n.", aqbq_node.name,
					strerror(errno));
			goto MmapAqBqFailed;
		}
		*(aqbq_node.addr + GPIO_TRI_REG) = GPIO_ALL_READ_MASK;
		*(aqbq_node.addr + GPIO2_TRI_REG) = GPIO_ALL_READ_MASK;
		pe.aq = aqbq_node.addr + GPIO_DATA_REG;
		pe.bq = aqbq_node.addr + GPIO2_DATA_REG;
		printf("%s - mapped at: %p\n", aqbq_node.name, aqbq_node.addr);

		if (!axi_mmap(mem_fd, &iadibd_node)) {
			printf("Failed mmap. Interface: %s.\n%s\n.", iadibd_node.name,
					strerror(errno));
			goto MmapIAdIBdFailed;
		}
		*(iadibd_node.addr + GPIO_TRI_REG) = GPIO_ALL_WRITE_MASK;
		*(iadibd_node.addr + GPIO2_TRI_REG) = GPIO_ALL_WRITE_MASK;
		pe.iad = iadibd_node.addr + GPIO_DATA_REG;
		pe.ibd = iadibd_node.addr + GPIO2_DATA_REG;
		printf("%s - mapped at: %p\n", iadibd_node.name, iadibd_node.addr);

		if (!axi_mmap(mem_fd, &adbd_node)) {
			printf("Failed mmap. Interface: %s.\n%s\n.", adbd_node.name,
					strerror(errno));
			goto MmapAdBdFailed;
		}
		*(adbd_node.addr + GPIO_TRI_REG) = GPIO_ALL_WRITE_MASK;
		*(adbd_node.addr + GPIO2_TRI_REG) = GPIO_ALL_WRITE_MASK;
		pe.ad = adbd_node.addr + GPIO_DATA_REG;
		pe.bd = adbd_node.addr + GPIO2_DATA_REG;
		printf("%s - mapped at: %p\n", adbd_node.name, adbd_node.addr);
	}

	return true;

MmapAdBdFailed:
	axi_munmap(&iadibd_node);
	pe.iad = pe.ibd = NULL;
MmapIAdIBdFailed:
	axi_munmap(&aqbq_node);
	pe.aq = pe.bq = NULL;
MmapAqBqFailed:
	axi_munmap(&iaqibq_node);
	pe.iaq = pe.ibq = NULL;
MmapIAqIBqFailed:
	axi_munmap(&qxqy_node);	
	pe.qx = pe.qy = NULL;
MmapQxQyFailed:
	axi_munmap(&xy_node);
	pe.x = pe.y = NULL;
MmapXYFailed:
	axi_munmap(&config_node);
	pe.config_reg = NULL;
MmapConfigFailed:
	return false;
}

bool pe_destruct() {
	if (!axi_munmap(&config_node)) {
		printf("%s - Could not unmap %p.\n%s\n", config_node.name,
				config_node.mapped_addr, strerror(errno));
		return false;
	}
	pe.config_reg = NULL;

	if (!axi_munmap(&xy_node)) {
		printf("%s - Could not unmap %p.\n%s\n", xy_node.name,
				xy_node.mapped_addr, strerror(errno));
		return false;
	}
	pe.x = pe.y = NULL;

	if (!axi_munmap(&qxqy_node)) {
		printf("%s - Could not unmap %p.\n%s\n", qxqy_node.name,
				qxqy_node.mapped_addr, strerror(errno));
		return false;
	}
	pe.qx = pe.qy = NULL;

	if (pe.debuggable) {
		if (!axi_munmap(&iaqibq_node)) {
			printf("%s - Could not unmap %p.\n%s\n", iaqibq_node.name,
					iaqibq_node.mapped_addr, strerror(errno));
			return false;
		}
		pe.iaq = pe.ibq= NULL;

		if (!axi_munmap(&aqbq_node)) {
			printf("%s - Could not unmap %p.\n%s\n", aqbq_node.name,
					aqbq_node.mapped_addr, strerror(errno));
			return false;
		}
		pe.aq = pe.bq = NULL;

		if (!axi_munmap(&iadibd_node)) {
			printf("%s - Could not unmap %p.\n%s\n", iadibd_node.name,
					iadibd_node.mapped_addr, strerror(errno));
			return false;
		}
		pe.iad = pe.ibd = NULL;

		if (!axi_munmap(&adbd_node)) {
			printf("%s - Could not unmap %p.\n%s\n", adbd_node.name,
					adbd_node.mapped_addr, strerror(errno));
			return false;
		}
		pe.ad = pe.bd = NULL;

	}
	
	return true;
}

void set_cw(uint32_t control_word) {
	assert(pe.config_reg != NULL);

	control_word = control_word & 0xFu;

	/* Also clearing: Clk = 0, and Rst = 0 */
	*(pe.config_reg) = control_word;
}

void set_xy(uint32_t x, uint32_t y) {
	assert(pe.x != NULL && pe.y != NULL && pe.x != pe.y);

	*(pe.x) = x;
	*(pe.y) = y;
}

void get_qxqy(uint32_t *qx, uint32_t *qy) {
	assert(qx != NULL && qy != NULL && qx != qy);
	assert(pe.qx != NULL && pe.qy != NULL && pe.qx != pe.qy);

	*qx = *(pe.qx);
	*qy = *(pe.qy);
}

bool force_regs(uint32_t iad, uint32_t ibd, uint32_t ad, uint32_t bd) {
	assert(pe.iad != NULL && pe.ibd != NULL && pe.ad != NULL && pe.bd != NULL);
	assert(pe.bd != pe.iad && pe.bd != pe.ibd && pe.bd != pe.ad);
	assert(pe.ad != pe.iad && pe.ad != pe.ibd);
	assert(pe.ibd != pe.iad);
	assert(pe.debuggable == true);

	if (!pe.debuggable)
		return false;

	*(pe.iad) = iad;
	*(pe.ibd) = ibd;
	*(pe.ad) = ad;
	*(pe.bd) = bd;

	*(pe.config_reg) |= DEBUG_WRITE_REGS_MASK;
	clock_pulse();
	*(pe.config_reg) &= ~DEBUG_WRITE_REGS_MASK;

	return true;
}

bool get_regs(uint32_t *iaq, uint32_t *ibq, uint32_t *aq, uint32_t *bq) {
	assert(iaq != NULL && ibq != NULL && aq != NULL && bq != NULL);
	assert(bq != iaq && bq != ibq && bq != aq);
	assert(aq != iaq && aq != ibq);
	assert(ibq != iaq);
	assert(pe.iaq != NULL && pe.ibq != NULL && pe.aq != NULL && pe.bq != NULL);
	assert(pe.bq != pe.iaq && pe.bq != pe.ibq && pe.bq != pe.aq);
	assert(pe.aq != pe.iaq && pe.aq != pe.ibq);
	assert(pe.aq != pe.iaq);

	if (!pe.debuggable) {
		*iaq = *ibq = *aq = *bq = 0u;
		return false;
	}

	*iaq = *(pe.iaq);
	*ibq = *(pe.ibq);
	*aq = *(pe.aq);
	*bq = *(pe.bq);
	return true;
}

void reset_pulse() {
	const struct timespec rst_ts = {0, CLK_PERIOD/2};

	assert(pe.config_reg != NULL);

	*(pe.config_reg) |= RESET_MASK;

	if (nanosleep(&rst_ts, NULL))
		fprintf(stderr,
				"\033[0;33mWARNING:\033[0m something went wrong during nanosleep.");

	*(pe.config_reg) &= ~RESET_MASK;

	if (nanosleep(&rst_ts, NULL))
		fprintf(stderr,
				"\033[0;33mWARNING:\033[0m something went wrong during nanosleep.");

}

void clock_pulse() {
	const struct timespec ts = {0, CLK_PERIOD/2};

	assert(pe.config_reg != NULL);

	*(pe.config_reg) |= CLK_MASK;

	if (nanosleep(&ts, NULL))
		fprintf(stderr,
				"\033[0;33mWARNING:\033[0m something went wrong during nanosleep.");

	*(pe.config_reg) &= ~CLK_MASK;

	if (nanosleep(&ts, NULL))
		fprintf(stderr,
				"\033[0;33mWARNING:\033[0m something went wrong during nanosleep.");
}
