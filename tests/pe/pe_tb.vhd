-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pe_tb is
end pe_tb;

architecture testbench of pe_tb is
	constant DATA_WIDTH	: integer := 32;
	constant CLK_PERIOD	: time := 10ns;

	component pe is
		Port (
			Clk			: in	std_logic;
			Rst			: in	std_logic;
			ControlWord	: in	std_logic_vector(3 downto 0);
			X, Y		: in	std_logic_vector(DATA_WIDTH-1 downto 0);
			Qx, Qy		: out	std_logic_vector(DATA_WIDTH-1 downto 0);

			DbgInEn			: in	std_logic := '0';
			DbgIAq, DbgIBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0);
			DbgAq, DbgBq	: out	std_logic_vector(DATA_WIDTH-1 downto 0);

			DbgIAd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgIBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgAd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-');
			DbgBd	: in	std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '-')
		);
	end component;

	signal Clk, Rst		: std_logic;
	signal ControlWord	: std_logic_vector(3 downto 0);
	signal X, Y, Qx, Qy	: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal IA, IB, A, B	: std_logic_vector(DATA_WIDTH-1 downto 0);

	type test_data is record
		ControlWord		: std_logic_vector(3 downto 0);
		X, Y, Qx, Qy	: std_logic_vector(DATA_WIDTH-1 downto 0);
	end record;

	type test_data_array is array (natural range<>) of test_data;
	constant test : test_data_array := (
		--Name		, CW  , X      , Y      , Qx     , Qy

		-- OUT<E/I<H>> tests --------------------------------------------------

		-- 3 + 6 -> B -> A = 6, B = 9
		-- "ADDEA"
		(x"4", x"00000003", x"00000003", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"00000003", x"00000006", x"00000000", x"00000000"),
		-- New X and Y should not be affecting Qx and Qy in OUTIH
		-- "OUTIH"
		(x"F", x"00000000", x"00000000", x"00000006", x"00000009"),
		-- "OUTIH"
		(x"F", x"00000000", x"0000FFFF", x"00000006", x"00000009"),
		-- "OUTIH"
		(x"F", x"FFFF0000", x"00000000", x"00000006", x"00000009"),
		-- "OUTIH"
		(x"F", x"FFFF0000", x"0000FFFF", x"00000006", x"00000009"),
		-- Original X and Y should be outputted as OUTIH should hold them.
		-- "OUTE"
		(x"E", x"0000FFFF", x"FFFF0000", x"00000003", x"00000006"),

		-- X and Y should not be affecting Qx and Qy in OUTI
		-- "OUTI"
		(x"D", x"00000000", x"00000000", x"00000006", x"00000009"),
		-- "OUTI"
		(x"D", x"00000000", x"0000FFFF", x"00000006", x"00000009"),
		-- "OUTI"
		(x"D", x"FFFF0000", x"00000000", x"00000006", x"00000009"),
		-- "OUTI"
		(x"D", x"FFFF0000", x"0000FFFF", x"00000006", x"00000009"),
		-- Qx and Qy should be the previous X and Y, unaffected by new X and Y
		-- "OUTE"
		(x"E", x"0000FFFF", x"FFFF0000", x"FFFF0000", x"0000FFFF"),

		-----------------------------------------------------------------------

		-- PASS test ----------------------------------------------------------

		-- Internal registers A and B should not be affected.
		-- "PASS"
		(x"C", x"60606060", x"06060606", x"60606060", x"06060606"),
		-- A and B were 1 and 0 from the addition tests.
		-- "OUTIH"
		(x"F", x"00000001", x"00000005", x"00000006", x"00000009"),
		-- "OUTE"
		(x"E", x"0000FFFF", x"FFFF0000", x"60606060", x"06060606"),

		-----------------------------------------------------------------------

		-- ADDE<A/B> tests ----------------------------------------------------

		-- Clear A and B & test addition of 0 with 0
		-- "ADDEA"
		(x"4", x"00000000", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"00000000", x"00000000", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"00000000", x"00000000"),

		-- "ADDEA"
		(x"4", x"FFFF0000", x"0000FFFF", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"66660000", x"00006666", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"FFFFFFFF", x"66666666"),

		-- "ADDEB"
		(x"6", x"FFFF0000", x"0000FFFF", x"00000000", x"00000000"),
		-- "ADDEA"
		(x"4", x"66660000", x"00006666", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"66666666", x"FFFFFFFF"), 

		-- Check addition with 0
		-- "ADDEA"
		(x"4", x"F0F0F0F0", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"06060606", x"00000000", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"F0F0F0F0", x"06060606"),

		-- "ADDEA"
		(x"4", x"00000000", x"06060606", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"00000000", x"F0F0F0F0", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"06060606", x"F0F0F0F0"),

		-- Check overflow
		-- "ADDEA"
		(x"4", x"FFFFFFFF", x"00000001", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"FFFFFFFE", x"00000003", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"00000000", x"00000001"),

		-- "ADDEA"
		(x"4", x"00000003", x"FFFFFFFE", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"00000001", x"FFFFFFFF", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"00000001", x"00000000"),

		-----------------------------------------------------------------------

		-- ADDI<A/B> test -----------------------------------------------------

		-- X and Y should not affect A and B
		-- IA and IB should be held


		-- Check addition with 0

		-- A = 0x60606060, B = 0x00000000
		-- IA = 0xF0F0F0F0, IB = 0x0F0F0F0FV
		-- "ADDEA"
		(x"4", x"60606060", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"00000000", x"00000000", x"00000000", x"00000000"),
		-- "PASS"
		(x"C", x"F0F0F0F0", x"0F0F0F0F", x"F0F0F0F0", x"0F0F0F0F"),

		-- "ADDIA"
		(x"5", x"55550000", x"00005555", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"50505050", x"05050505", x"60606060", x"00000000"),

		-- "ADDIB"
		(x"7", x"50505050", x"05050505", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"F0F0F0F0", x"0F0F0F0F", x"60606060", x"60606060"),

		-- Original IA and IB should have been held.
		-- "OUTE"
		(x"E", x"05050505", x"50505050", x"F0F0F0F0", x"0F0F0F0F"),


		-- A = 0x00000000, B = 0x60606060
		-- IA = 0x0F0F0F0F, IB = 0xF0F0F0F0
		-- "ADDEA"
		(x"4", x"00000000", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"00000000", x"60606060", x"00000000", x"00000000"),
		-- "PASS"
		(x"C", x"0F0F0F0F", x"F0F0F0F0", x"0F0F0F0F", x"F0F0F0F0"),

		-- "ADDIB"
		(x"7", x"05050505", x"50505050", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"F0F0F0F0", x"0F0F0F0F", x"00000000", x"60606060"),

		-- "ADDIA"
		(x"5", x"50505050", x"05050505", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"50505050", x"05050505", x"60606060", x"60606060"),

		-- Original IA and IB should have been held.
		-- "OUTE"
		(x"E", x"50505050", x"05050505", x"0F0F0F0F", x"F0F0F0F0"),


		-- Check addition and overflow

		-- A = 0x60606060, B = 60606060 from previous test set
		-- IA = 0xF0F0F0F0, IB = 0x0F0F0F0F
		-- "PASS"
		(x"C", x"F0F0F0F0", x"0F0F0F0F", x"F0F0F0F0", x"0F0F0F0F"),

		-- "ADDIA"
		(x"5", x"55550000", x"00005555", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"50505050", x"05050505", x"C0C0C0C0", x"60606060"),

		-- "ADDIB"
		(x"7", x"50505050", x"05050505", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"F0F0F0F0", x"0F0F0F0F", x"C0C0C0C0", x"21212120"),

		-- Original IA and IB should have been held.
		-- "OUTE"
		(x"E", x"05050505", x"50505050", x"F0F0F0F0", x"0F0F0F0F"),

		-- A = 0xC0C0C0C0, B = 0x21212120
		-- IA = 0xF0F0F0F0, IB = 0x0F0F0F0F
		-- "PASS"
		(x"C", x"F0F0F0F0", x"0F0F0F0F", x"F0F0F0F0", x"0F0F0F0F"),

		-- "ADDIB"
		(x"7", x"05050505", x"50505050", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"F0F0F0F0", x"0F0F0F0F", x"C0C0C0C0", x"E1E1E1E0"),

		-- "ADDIA"
		(x"5", x"50505050", x"05050505", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"50505050", x"05050505", x"A2A2A2A0", x"E1E1E1E0"),

		-- "ADDIB"
		(x"7", x"05050505", x"50505050", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"F0F0F0F0", x"0F0F0F0F", x"A2A2A2A0", x"84848480"),

		-- Original IA and IB should have been held.
		-- "OUTE"
		(x"E", x"50505050", x"05050505", x"F0F0F0F0", x"0F0F0F0F"),

		-----------------------------------------------------------------------

		-- MULE<A/B> tests ----------------------------------------------------

		-- Clear A and B
		-- "ADDEA"
		(x"4", x"00000000", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"00000000", x"00000000", x"00000000", x"00000000"),

		-- Test multiplication

		-- "MULEA"
		(x"0", x"11111111", x"0000000F", x"00000000", x"00000000"),
		-- "MULEB"
		(x"2", x"00000005", x"11111111", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"FFFFFFFF", x"55555555"),

		-- "MULEB"
		(x"2", x"0000000F", x"11111111", x"00000000", x"00000000"),
		-- "MULEA"
		(x"0", x"11111111", x"00000005", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"55555555", x"FFFFFFFF"),

		-- "MULEA"
		(x"0", x"10101010", x"0000000F", x"00000000", x"00000000"),
		-- "MULEB"
		(x"2", x"00000005", x"01010101", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"F0F0F0F0", x"05050505"),

		-- "MULEB"
		(x"2", x"0000000F", x"10101010", x"00000000", x"00000000"),
		-- "MULEA"
		(x"0", x"00000005", x"01010101", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"05050505", x"F0F0F0F0"),

		-- "ADDEA"
		(x"0", x"55555555", x"00000002", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"2", x"00000002", x"7FFFFFFF", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"AAAAAAAA", x"FFFFFFFE"),

		-- "ADDEB"
		(x"2", x"55555555", x"00000002", x"00000000", x"00000000"),
		-- "ADDEA"
		(x"0", x"00000002", x"7FFFFFFF", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"FFFFFFFE", x"AAAAAAAA"),

		-- Check multiplication with 0

		-- "MULEA"
		(x"0", x"0F0F0F0F", x"00000000", x"00000000", x"00000000"),
		-- "MULEB"
		(x"2", x"00000000", x"50505050", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"00000000", x"00000000"),

		-- "MULEB"
		(x"2", x"0F0F0F0F", x"00000000", x"00000000", x"00000000"),
		-- "MULEA"
		(x"0", x"00000000", x"50505050", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"00000000", x"00000000"),

		-- Check overflow

		-- "MULEA"
		(x"0", x"88888888", x"00000002", x"00000000", x"00000000"),
		-- "MULEB"
		(x"2", x"0000000F", x"FFFFFFFF", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"11111110", x"FFFFFFF1"),

		-- "MULEA"
		(x"2", x"88888888", x"00000002", x"00000000", x"00000000"),
		-- "MULEB"
		(x"0", x"0000000F", x"FFFFFFFF", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"F0F0F0F0", x"0F0F0F0F", x"FFFFFFF1", x"11111110"),

		-----------------------------------------------------------------------

		-- MULI<A/B> test -----------------------------------------------------

		-- X and Y should not affect A and B
		-- IA and IB should be held


		-- Check multiplication with 0

		-- A = 0x50505050, B = 0x00000000
		-- IA = 0xF0F0F0F0, IB = 0x0F0F0F0F
		-- "ADDEA"
		(x"4", x"50505050", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"00000000", x"00000000", x"00000000", x"00000000"),
		-- "PASS"
		(x"C", x"F0F0F0F0", x"0F0F0F0F", x"F0F0F0F0", x"0F0F0F0F"),

		-- "MULIA"
		(x"1", x"AAAA0000", x"0000AAAA", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"A0A0A0A0", x"0A0A0A0A", x"00000000", x"00000000"),

		-- "MULIB"
		(x"3", x"A0A0A0A0", x"0A0A0A0A", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"F0F0F0F0", x"0F0F0F0F", x"00000000", x"00000000"),

		-- Original IA and IB should have been held.
		-- "OUTE"
		(x"E", x"0A0A0A0A", x"A0A0A0A0", x"F0F0F0F0", x"0F0F0F0F"),


		-- A = 0x00000000, B = 0x50505050
		-- IA = 0x0F0F0F0F, IB = 0xF0F0F0F0
		-- "ADDEA"
		(x"4", x"00000000", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"00000000", x"50505050", x"00000000", x"00000000"),
		-- "PASS"
		(x"C", x"0F0F0F0F", x"F0F0F0F0", x"0F0F0F0F", x"F0F0F0F0"),

		-- "MULIB"
		(x"3", x"0A0A0A0A", x"A0A0A0A0", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"F0F0F0F0", x"0F0F0F0F", x"00000000", x"00000000"),

		-- "MULIA"
		(x"1", x"A0A0A0A0", x"0A0A0A0A", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"50505050", x"05050505", x"00000000", x"00000000"),

		-- Original IA and IB should have been held.
		-- "OUTE"
		(x"E", x"50505050", x"05050505", x"0F0F0F0F", x"F0F0F0F0"),


		-- Check multiplication and overflow

		-- A = 0xFFFFFFFF, B = 0x0000000F
		-- IA = 0x50505050, IB = 0x0505050
		-- "ADDEA"
		(x"4", x"FFFFFFFF", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"0000000F", x"00000000", x"00000000", x"00000000"),
		-- "PASS"
		(x"C", x"50505050", x"05050505", x"50505050", x"05050505"),

		-- "MULIA"
		(x"1", x"A0A0A0A0", x"0A0A0A0A", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"0A0A0A0A", x"A0A0A0A0", x"FFFFFFF1", x"0000000F"),

		-- "MULEB"
		(x"3", x"A0A0A0A0", x"0A0A0A0A", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"0A0A0A0A", x"A0A0A0A0", x"FFFFFFF1", x"FFFFFF1F"),

		-- Original IA and IB should have been held.
		-- "OUTE"
		(x"E", x"F0F0F0F0", x"0F0F0F0F", x"50505050", x"05050505"),

		-- A = 0x0000000F, B = 0xFFFFFFFF
		-- IA = 0x05050505, IB = 0x50505050
		-- "ADDEA"
		(x"4", x"0000000F", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"FFFFFFFF", x"00000000", x"00000000", x"00000000"),
		-- "PASS"
		(x"C", x"05050505", x"50505050", x"05050505", x"50505050"),

		-- "MULEB"
		(x"3", x"A0A0A0A0", x"0A0A0A0A", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"0A0A0A0A", x"A0A0A0A0", x"0000000F", x"FFFFFFF1"),

		-- "MULIA"
		(x"1", x"A0A0A0A0", x"0A0A0A0A", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"0A0A0A0A", x"A0A0A0A0", x"FFFFFF1F", x"FFFFFFF1"),


		-- Original IA and IB should have been held.
		-- "OUTE"
		(x"E", x"0F0F0F0F", x"F0F0F0F0", x"05050505", x"50505050"),

		-----------------------------------------------------------------------

		-- SWAPG test ---------------------------------------------------------

		-- A = 0xF0F0F0F0, B = 0x0F0F0F0F
		-- "ADDEA"
		(x"4", x"F0F0F0F0", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"0F0F0F0F", x"00000000", x"00000000", x"00000000"),

		-- "SWAPG" - 0x8
		(x"8", x"05050505", x"50505050", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"0A0A0A0A", x"A0A0A0A0", x"05050505", x"50505050"),

		-- "SWAPG" - 0x9
		(x"9", x"0A0A0A0A", x"A0A0A0A0", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"50505050", x"05050505", x"0A0A0A0A", x"A0A0A0A0"),

		-- "SWAPG" - 0x8
		(x"8", x"50505050", x"05050505", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"A0A0A0A0", x"0A0A0A0A", x"05050505", x"50505050"),

		-- "SWAPG" - 0x9
		(x"8", x"A0A0A0A0", x"0A0A0A0A", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"05050505", x"50505050", x"0A0A0A0A", x"A0A0A0A0"),

		-- Should be the last IA and IB held
		-- "OUTE"
		(x"E", x"F0F0F0F0", x"0F0F0F0F", x"A0A0A0A0", x"0A0A0A0A"),

		-----------------------------------------------------------------------

		-- SWAPGH test --------------------------------------------------------

		-- A = 0x0F0F0F0F, B = 0xF0F0F0F0
		-- "ADDEA"
		(x"4", x"0F0F0F0F", x"00000000", x"00000000", x"00000000"),
		-- "ADDEB"
		(x"6", x"F0F0F0F0", x"00000000", x"00000000", x"00000000"),
		-- "PASS"
		(x"C", x"50505050", x"05050505", x"50505050", x"05050505"),

		-- "SWAPGH" - 0xA
		(x"A", x"A0A0A0A0", x"0A0A0A0A", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"0A0A0A0A", x"A0A0A0A0", x"05050505", x"50505050"),

		-- Previous IA and IB are held and used.
		-- "SWAPGH" - 0xB
		(x"B", x"50505050", x"05050505", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"50505050", x"05050505", x"0A0A0A0A", x"A0A0A0A0"),

		-- Previous IA and IB are held and used.
		-- "SWAPGH" - 0xA
		(x"A", x"A0A0A0A0", x"0A0A0A0A", x"00000000", x"00000000"),
		-- "OUTI"
		(x"D", x"A0A0A0A0", x"0A0A0A0A", x"05050505", x"50505050"),

		-- "SWAPGH" - 0xB
		(x"B", x"50505050", x"05050505", x"00000000", x"00000000"),
		-- "OUTIH"
		(x"F", x"05050505", x"50505050", x"0A0A0A0A", x"A0A0A0A0"),

		-- "OUTE"
		(x"E", x"F0F0F0F0", x"0F0F0F0F", x"A0A0A0A0", x"0A0A0A0A")

		-----------------------------------------------------------------------

	);

begin
	UUT: pe
		port map (Clk => Clk, Rst => Rst,
				  ControlWord => ControlWord,
				  X => X, Y => Y,
				  Qx => Qx, Qy => Qy,
				  DbgIAq => IA,	DbgIBq => IB, DbgAq => A, DbgBq => B);

	Clock: process
	begin
		Clk <= '0';
		wait for CLK_PERIOD/2;
		Clk <= '1';
		wait for CLK_PERIOD/2;
	end process;


	process
	begin
		for i in test'range loop
			ControlWord <= test(i).ControlWord;
			X <= test(i).X;
			Y <= test(i).Y;

			-- Zedboard does not seem to support falling edge trigger.
			if (i = 0) then
				Rst <= '1';
				wait for CLK_PERIOD/4;
				Rst <= '0';
				wait for CLK_PERIOD/4;
				wait for CLK_PERIOD/2;
				wait for CLK_PERIOD;
			else
				wait for 2*CLK_PERIOD;
			end if;
			
			assert ((Qx = test(i).Qx) and (Qy = test(i).Qy))
			report   ("Actual outputs differ from expected - " &
					"Actual " &
					"Qx: " & integer'image(to_integer(unsigned(Qx))) &
					", Qy: " & integer'image(to_integer(unsigned(Qy))) &
					" Expected " &
					"Qx: " & integer'image(to_integer(unsigned(test(i).Qx))) &
					", Qy: " & integer'image(to_integer(unsigned(test(i).Qy))))
			severity failure;
		end loop;
			
		wait;
	end process;

end testbench;
