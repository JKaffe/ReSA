-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

-- Test the correctness of X, Y, Qx, Qy connections.
-- PASS control word is used to test that unique inputs reach the corrects outputs.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pe_array_in_out_integrity_tb is
end pe_array_in_out_integrity_tb;

architecture testbench of pe_array_in_out_integrity_tb is
	constant CLK_PERIOD : time := 10ns;

	constant DATA_WIDTH						: integer	:= 32;
	constant N_BY_N							: integer	:= 8;
	constant ONLY_POSITIVE_EDGE_TRIGGERABLE	: boolean	:= true;

	component pe_array is
		generic (
			DATA_WIDTH						: integer	:= DATA_WIDTH;
			N_BY_N							: integer	:= N_BY_N;
			ONLY_POSITIVE_EDGE_TRIGGERABLE	: boolean	:= ONLY_POSITIVE_EDGE_TRIGGERABLE
		);
		port (
			Clk			: in	std_logic;
			Rst			: in	std_logic;
			ControlWord	: in	std_logic_vector(3 downto 0);
			in_top		: in	std_logic_vector(DATA_WIDTH*N_BY_N-1 DOWNTO 0);
			in_left		: in	std_logic_vector(DATA_WIDTH*N_BY_N-1 DOWNTO 0);
			out_bottom	: out	std_logic_vector(DATA_WIDTH*N_BY_N-1 DOWNTO 0);
			out_right	: out	std_logic_vector(DATA_WIDTH*N_BY_N-1 DOWNTO 0)
		);
	end component;

	signal Clk, Rst					: std_logic;
	signal ControlWord				: std_logic_vector(3 downto 0);
	signal in_top, in_left			: std_logic_vector(DATA_WIDTH*N_BY_N-1 downto 0);
	signal out_bottom, out_right	: std_logic_vector(DATA_WIDTH*N_BY_N-1 downto 0);
begin
	UUT: pe_array port map (
		Clk => Clk, Rst => Rst,	ControlWord => ControlWord,
		in_top => in_top, in_left => in_left,
		out_bottom => out_bottom, out_right => out_right
	);

	Clock: process
	begin
		Clk <= '0';
		wait for CLK_PERIOD/2;
		Clk <= '1';
		wait for CLK_PERIOD/2;
	end process;

	-- CW: PASS (0xC)
	--
	--        0x0 0x1
	--        ↓   ↓
	-- 0x2 → PE0 PE1 → 0x2
	-- 0x3 → PE2 PE3 → 0x3
	--        ↓   ↓
	--       0x0 0x1

	Test: process
	begin
		Rst <= '1';
		wait for CLK_PERIOD/4;
		Rst <= '0';

		ControlWord <= x"C";
		SetInputs: for i in 0 to N_BY_N*2 - 1 loop
			if (i <= N_BY_N - 1) then
				-- Set top side inputs
				in_top(DATA_WIDTH*(i+1)-1 downto DATA_WIDTH*i)
					<= std_logic_vector(to_signed(i, DATA_WIDTH));
			else
				-- Set left side inputs
				in_left(DATA_WIDTH*(i-N_BY_N+1)-1 downto DATA_WIDTH*(i-N_BY_N))
					<= std_logic_vector(to_signed(i, DATA_WIDTH));
			end if;
		end loop;

		wait for CLK_PERIOD/4;
		wait for CLK_PERIOD/2;

		wait for CLK_PERIOD*(N_BY_N-1);

		if (ONLY_POSITIVE_EDGE_TRIGGERABLE) then
			wait for CLK_PERIOD*(N_BY_N - 1);
		end if;

		assert (out_right = in_left and out_bottom = in_top)
		report	"Outputs do not match the inputs. " & 
				"Something posibly wrong with X and Qx, Y and Qy connections."
		severity failure;

		wait;
	end process;
end testbench;
