-- PackageName: ReSA
-- SPDX-FileCopyrightText: 2022 Karmjit Mahil
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity one_hot_selector_tb is
end one_hot_selector_tb;

architecture testbench of one_hot_selector_tb is
	constant CLK_PERIOD	: time		:= 10 ns;
	constant SIZE		: integer	:= 8;

	component one_hot_selector is
		generic (
			SIZE	: integer := SIZE
		);
		port (
			Clk	: in	std_logic;
			Rst	: in	std_logic;
			Q	: out	std_logic_vector(SIZE-1 downto 0)
		);
	end component;

	signal Rst, Clk	: std_logic;
	signal Q		: std_logic_vector(SIZE-1 downto 0);
begin
	UUT: one_hot_selector port map (Clk => Clk, Rst => Rst, Q => Q);

	Clock: process
	begin
		Clk <= '0';
		wait for CLK_PERIOD/2;
		Clk <= '1';
		wait for CLK_PERIOD/2;
	end process;

	process
	begin
		Rst <= '1';
		wait for CLK_PERIOD;
		Rst <= '0';

		wait for CLK_PERIOD*(SIZE*2+1);
	end process;
end testbench;
